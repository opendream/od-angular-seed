
/*
 * GET angular page.
 */

exports.render = function(req, res){
  res.render('partials/' + req.params.name + '.handlebars', { layout: false });
};
