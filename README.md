#Install
    brew install node
    npm install -g bower
    npm install -g less
    gem install

    npm install
    cd public/
    bower install
    
#Run
    node app.js

#Compile less (only style.less)
    gem install watchr
    cd public/stylesheets/
    watchr -e 'watch("style.less") { |f| system("lessc #{f[0]} > style.css") }'
    or
    sh run-less.sh

#Angular
    use polate provider [[ variable ]]
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');