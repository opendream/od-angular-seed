/* Load this script using conditional IE comments if you need to support IE 7 and IE 6. */

window.onload = function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'icomoon\'">' + entity + '</span>' + html;
	}
	var icons = {
			'icon-newspaper' : '&#xe000;',
			'icon-grin' : '&#xe001;',
			'icon-smiley' : '&#xe002;',
			'icon-smiley-2' : '&#xe003;',
			'icon-tongue' : '&#xe004;',
			'icon-tongue-2' : '&#xe005;',
			'icon-sad' : '&#xe006;',
			'icon-wink' : '&#xe007;',
			'icon-wink-2' : '&#xe008;',
			'icon-sad-2' : '&#xe009;',
			'icon-cool' : '&#xe00a;',
			'icon-cool-2' : '&#xe00b;',
			'icon-angry' : '&#xe00c;',
			'icon-angry-2' : '&#xe00d;',
			'icon-grin-2' : '&#xe00e;',
			'icon-evil' : '&#xe00f;',
			'icon-evil-2' : '&#xe010;',
			'icon-shocked' : '&#xe011;',
			'icon-happy' : '&#xe012;',
			'icon-happy-2' : '&#xe013;',
			'icon-shocked-2' : '&#xe014;',
			'icon-confused' : '&#xe015;',
			'icon-neutral' : '&#xe016;',
			'icon-confused-2' : '&#xe017;',
			'icon-neutral-2' : '&#xe018;',
			'icon-wondering' : '&#xe019;',
			'icon-wondering-2' : '&#xe01a;'
		},
		els = document.getElementsByTagName('*'),
		i, attr, html, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		attr = el.getAttribute('data-icon');
		if (attr) {
			addIcon(el, attr);
		}
		c = el.className;
		c = c.match(/icon-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
};