"use strict";

angular.module('cityemoApp')
  .directive('heatmap', function (getRate) {
    return {
      restrict: 'E',
      replace: true,
      template: "<div id=\"map-canvas-roadmap\"></div>",
      link: function postLink(scope, element, attrs) {

        var bangkok = new google.maps.LatLng(13.752508350428734, 100.5451202392578);
        var chiangmai = new google.maps.LatLng(18.788646, 98.98613);

        var mapOptions = {
            center: chiangmai,
            zoom: 14,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            streetViewControl: false,
            linksControl: false,
            mapTypeControl: false
          };

        var map = new google.maps.Map($('#map-canvas-roadmap')[0], mapOptions);

        getRate.get_rate(function (response) {
          // var markers = [];
          if (response.status == 'ok') {
            _.each(response.data, function (value, key) {
              add_marker(value['position']['lat'], value['position']['lng'], value.rate);
            });
          }
          // var markerCluster = new MarkerClusterer(map, markers);
        });

        function add_marker(lat, lng, rate) {
          var iconBase = 'images/';
          var icons = {
            good: {
              icon: iconBase + 'pin-green.png',
            },
            bad: {
              icon: iconBase + 'pin-orange.png',
            },
            soso: {
              icon: iconBase + 'pin-red.png',
            }
          };
          var latLng = new google.maps.LatLng(lat, lng);
          var marker = new google.maps.Marker({
            position: latLng,
            map: map,
            icon: icons[rate].icon,
          });
        }
      }
    }
  });
