"use strict";

angular.module('cityemoApp')
  .directive('slidermenu', function ($location) {
    return {
      restrict: 'AE',
      replace: true,
      template: "<div class=\"menu-wrapper\"><div class=\"menu-title\" ng-hide=\"show\" ng-click=\"show = !show\"><span class=\"glyphicon glyphicon-list\"></span></div><div class=\"menu-list\" ng-show=\"show\"><span ng-click=\"show=false\" class=\"glyphicon glyphicon-remove close\"></span><div class=\"menu-text-title\">Menu</div><ul class=\"slider-menu\"><li class=\"[[menu.addClass]] [[hover]]\" ng-repeat=\"menu in menus\" ng-mouseover=\"hover='hover'\" ng-mouseleave=\"hover=''\"><a href=\"[[menu.href]]\" target=\"[[menu.target]]\">[[menu.name]]</a></li></ul></div></div>",
      scope: {
        show: "@"
      },
      link: function postLink(scope, element, attrs) {
        scope.show = true;
        var menus = [
          { name: "CityEmo", href: "cityemo", target: "", addClass: ""},
          { name: "Heat Map", href: "heatmap", target: "", addClass: ""},
          // { name: "Create by", href: "", target: ""},
        ];

        _.each(menus, function (value, key) {
          if ('/' + value.href == $location.path()) {
            value.addClass = 'active';
          }
        });

        scope.menus = menus;

        $('.menu-title').hover(
          function () {
            $(this).addClass('hover');
          },
          function () {
            $(this).removeClass('hover');
          }
        );

        scope.$watch('show', function (value) {
          if (value) {
            $('.view-wrapper').addClass('slide');
          }
          else {
            $('.view-wrapper').removeClass('slide');
          }
        });

        $('html').click(function() {
          scope.show = false;
          scope.$apply();
        });

        $(element).click(function(event){
          event.stopPropagation();
        });
      }
    }
  });
