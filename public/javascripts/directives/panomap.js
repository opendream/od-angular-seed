"use strict";

angular.module('cityemoApp')
  .directive('panomap', function () {
    return {
      restrict: 'E',
      replace: true,
      template: "<div class=\"map\"><div id=\"map-canvas\"></div>",
      scope: {
        shouldChangeMap: '=',
      },
      link: function postLink(scope, element, attrs) {

        scope.$watch('shouldChangeMap', function (value) {
          if (value) {
            next_random_place();
            scope.shouldChangeMap = false;
            scope.$emit("streetview", streetview);
          }
        });

        var bangkok = new google.maps.LatLng(13.752508350428734, 100.5451202392578);
        var chiangmai = new google.maps.LatLng(18.788646, 98.98613);
        var leftTop = [18.89782, 98.934631];
        var rightBottom = [18.739861, 99.023209];

        chiangmai = random_in_the_box(leftTop, rightBottom);

        var panoramaOptions = {
          overviewMapControl: true,
          overviewMapControlOptions: {
            opened: true,
          },
          linksControl: false,
          position: chiangmai,
          pov: {
            heading: random(0, 360),
            pitch: 0
          },
          zoom: 1,
        };

        var streetview = new google.maps.StreetViewPanorama($('#map-canvas')[0], panoramaOptions);
        var streetService = new google.maps.StreetViewService();
        var checkaround = 1000;
        next_random_place();

        function next_random_place() {
          var randomPoint = random_in_the_box(leftTop, rightBottom);
          streetService.getPanoramaByLocation(randomPoint, checkaround, function (data) {
              if (data == null) {
                return next_random_place();
              }
              streetview.setPosition(data.location.latLng);
              streetview.setPov({
                heading: random(0, 360),
                pitch: 0
              });
              //streetview.setVisible(true);

              return data;
          });
        }
        
        scope.$emit("streetview", streetview);

        function random(start, to, makeInt) {
          start = start || 0;
          to = to || 1;
          makeInt = (makeInt === false) ? makeInt : true;

          var max = to - start;
          var random = (Math.random() * Math.pow(10, parseInt(max).toString().length)) % max;
          return start + (makeInt ? parseInt(random) : random);
        }

        function random_in_the_box(leftTop, rightBottom) {
          var lat = random(rightBottom[0], leftTop[0], false); 
          var lng = random(leftTop[1], rightBottom[1], false);

          return new google.maps.LatLng(lat, lng);
        }
      }
    }
  });
