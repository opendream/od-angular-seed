jQuery(document).ready(function ($) {

function initialize() {
  var bangkok = new google.maps.LatLng(13.752508350428734, 100.5451202392578);
  var chiangmai = new google.maps.LatLng(18.788646, 98.98613);
  var leftTop = [18.89782, 98.934631];
  var rightBottom = [18.739861, 99.023209];

  var panoramaOptions = {
    overviewMapControl: true,
    overviewMapControlOptions: {
      opened: true,
    },
    linksControl: false,
    position: chiangmai,
    pov: {
      heading: random(0, 360),
      pitch: 0
    },
    zoom: 1,
  };

  var streetview = new google.maps.StreetViewPanorama($('#map-canvas')[0], panoramaOptions);
  var streetService = new google.maps.StreetViewService();
  var checkaround = 1000;

  $('#good').click(function (e) {
    sendRating('good');
  });

  $('#soso').click(function (e) {
    sendRating('soso');
  });

  $('#bad').click(function (e) {
    sendRating('bad');
  });

  function sendRating(rate) {
    var position = streetview.getPosition();
    position = {
      lat: position.mb,
      lng: position.nb
    };

    var pov = streetview.getPov();
    pov = {
      heading: pov.heading,
      pitch: pov.pitch,
      zoom: pov.zoom
    };

    $.getJSON('/rate', {position: position, pov: pov, rate: rate}, function () {
        nextRandomPlace();
    });
  }

  function nextRandomPlace() {
    var randomPoint = randomInTheBox(leftTop, rightBottom);
    streetService.getPanoramaByLocation(randomPoint, checkaround, function (data) {
        if (data == null) {
          return nextRandomPlace();
        }

        streetview.setPosition(data.location.latLng);
        streetview.setPov({
          heading: random(0, 360),
          pitch: 0
        });
        //streetview.setVisible(true);

        return data;
    });
  }

  nextRandomPlace();
}

google.maps.event.addDomListener(window, 'load', initialize);

function random(start, to, makeInt) {
  start = start || 0;
  to = to || 1;
  makeInt = (makeInt === false) ? makeInt : true;

  var max = to - start;
  var random = (Math.random() * Math.pow(10, parseInt(max).toString().length)) % max;
  return start + (makeInt ? parseInt(random) : random);
}

function randomInTheBox(leftTop, rightBottom) {
  var lat = random(rightBottom[0], leftTop[0], false); 
  var lng = random(leftTop[1], rightBottom[1], false);

  return new google.maps.LatLng(lat, lng);
}

});

