'use strict';

angular.module('angularApp',['ngRoute', 'ngResource', 'ngAnimate'])
  .config(function ($routeProvider, $locationProvider, $interpolateProvider) {
    $locationProvider.html5Mode(true);
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
    $routeProvider
      .when('/test', {
        templateUrl: 'partials/testpage',
        controller: 'homeCtrl'
      })
      .otherwise({
        redirectTo: '/',
        controller: 'homeCtrl'
      });
    })
