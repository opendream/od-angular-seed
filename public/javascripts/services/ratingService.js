"use strict";

angular.module('cityemoApp')
  .factory('RatingService', function ($rootScope, $resource, $timeout) {
    var Rate = $resource('/get/rate');
    var responses = [ ];

    var poller = function() {
      console.log("polling");
        Rate.get({}, function (response, getReponseHeaders) {
          // callback(response);
          responses = response;
          $timeout(poller, 1000);
        },
        function (response, getReponseHeaders) {
          responses = response;
          // callback(response);
          $timeout(poller, 1000);
        }
      );
    }

    poller();
    
    return {
      get_rate: function (callback) {
      },
      get_responses: function() {
        return responses;
      }
    };
  });
