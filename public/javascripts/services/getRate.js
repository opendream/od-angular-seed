"use strict";

angular.module('cityemoApp')
  .factory('getRate', function ($rootScope, $resource) {
    return {
      get_rate: function (callback) {

        var Rate = $resource('/get/rate');
        var send_rate = Rate.get({}, 
          function (response, getReponseHeaders) {
            callback(response);
          },
          function (response, getReponseHeaders) {
            callback(response);
          }
        );
      }
    };
  });
