"use strict";

angular.module('cityemoApp')
  .factory('sendRate', function ($rootScope, $resource) {
    return {
      send_rate: function (rate, position, pov) {
        position = {
          lat: position.mb,
          lng: position.nb
        };

        pov = {
          heading: pov.heading,
          pitch: pov.pitch,
          zoom: pov.zoom
        };

        $rootScope.$broadcast('next_random_place', true);
        var Send_rate = $resource('/rate');
        var send_rate = Send_rate.get({position: position, pov: pov, rate: rate}, 
          function (response, getReponseHeaders) {
            // $rootScope.$broadcast('next_random_place', true);
          },
          function (response, getReponseHeaders) {
            // $rootScope.$broadcast('next_random_place', false);
          }
        );
      }
    };
  });
